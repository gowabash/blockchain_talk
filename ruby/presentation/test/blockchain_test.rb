require_relative 'test_helper'

class BlockChainTest < Minitest::Test
  def test_can_create_chain
    priv, pub = Helper.keys
    bc = Blockchain.new(pub)
    assert bc
  end

  def test_can_count_blocks
    priv, pub = Helper.keys
    bc = Blockchain.new(pub)
    assert_equal 1, bc.count 
  end

  def test_can_add_block
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    blockchain = Blockchain.new(pub1)
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    signature = key.sign(digest, blockchain.last_block.hash_val)
    t = Transaction.new(pub1, pub2, 100)

    b = Block.new(blockchain.last_block.hash_val, t, signature)
    b.mine!

    assert blockchain.add(b)
  end

  def test_cant_add_invalid_block
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    blockchain = Blockchain.new(pub1)
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    signature = key.sign(digest, blockchain.last_block.hash_val)
    t = Transaction.new(pub1, pub2, 100)

    b = Block.new(blockchain.last_block.hash_val, t, signature)

    exception = assert_raises("InvalidBlock") do
      blockchain.add(b)
    end
    assert_equal "Your block is not valid", exception.message
  end

  def test_cant_add_block_with_wrong_hash
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    blockchain = Blockchain.new(pub1)
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    fake_hash = 'sdfaksd09089'
    signature = key.sign(digest, fake_hash)
    t = Transaction.new(pub1, pub2, 100)

    b = Block.new(fake_hash, t, signature)
    b.mine!

    exception = assert_raises("InvalidBlock") do
      blockchain.add(b)
    end
    assert_equal "Previous block is incorrect", exception.message
  end

  def test_can_validate_chain
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    blockchain = Blockchain.new(pub1)
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    10.times do 
      signature = key.sign(digest, blockchain.last_block.hash_val)
      t = Transaction.new(pub1, pub2, 100)

      b = Block.new(blockchain.last_block.hash_val, t, signature)
      b.mine!
      blockchain.add(b)
    end
    assert blockchain.valid?
  end
end
