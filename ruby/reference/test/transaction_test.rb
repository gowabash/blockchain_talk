require 'minitest/autorun'
require_relative '../transaction'
require 'openssl'

class TransactionTest < Minitest::Test
    def test_transaction_creation
      t = Transaction.new('a', 'b', 100)
      assert_equal 'a', t.from
      assert_equal 'b', t.to
      assert_equal 100, t.amount
    end
end
