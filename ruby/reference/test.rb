#! /usr/bin/env ruby

#require 'bundler'
#Bundler.require
require 'awesome_print'
require_relative 'block'
require_relative 'transaction'

blockchain = []
genesis = Block.genesis

blockchain << genesis
4.times do
  t = Transaction.new('matt', 'janette', Random.new.rand(10000), nil)
  block = Block.new(genesis, t)
  block.mine!
  blockchain << block
end

blockchain.each do |i|
  puts i
  puts '-' * 30 unless blockchain.last == i
end
