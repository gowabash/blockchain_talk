class Blockchain
  def initialize(pubkey)
    @the_chain = [Block.genesis(pubkey)]
  end

  def count
    @the_chain.count
  end

  def add(block)
    validate(block)
    @the_chain << block
  end

  def last_block
    return @the_chain.last
  end

  def valid?
    (1..@the_chain.length - 1).each do |i|
      block = @the_chain[i]
      prev_block = @the_chain[i-1]
      valid = block.valid?
      orderly = prev_block.hash_val == block.prev_block_hash
      valid && orderly
    end
  end

  private 

  def in_order?(block)
    block.prev_block_hash == last_block.hash_val
  end

  def validate(block)
    if block.valid?
      if in_order?(block)
        return true
      else
        raise InvalidBlock.new("Previous block is incorrect")
      end
    else
      raise InvalidBlock.new("Your block is not valid")
    end
  end
end

class InvalidBlock < StandardError
end
