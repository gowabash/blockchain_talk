# Notes, code and keynote for presentation on Bitcoin
* Abstract
  * Bitcoin and cryptocurrency is all the rage. Many industries are under attack from technology companies using a distributed ledger (fintech, healthcare, IOT, ...). Lots of money is being made (and likely lost) in this new currency and most people don't even understand what is happening. We will work to de-mystify the math and computing behind the currency and then use our new found knowledge to build a simple implementation in python using TDD principles.
  * Participants in this workshop should have an interest in distributed computing.    We will discuss the technical aspects of cryptocurrencies including fault tolerance, identity, double spending, mining, and then will implement a simple block chain.  
* Bio
  * Matt Brown is CTO at Pattern89, a marketing R&D platform for digital ads, where he leads a team of engineers who help marketers experiment at scale. In his past, he was a director for both Salesforce and ExactTarget. Those positions came after the acquisition of iGoDigital where he was VP of Engineering. He has interests in many things including ruby, good beverages (coffee and beer mostly) as well as t-shirts (tri-blend for the win).

