#! /usr/bin/env ruby

require 'json'
require 'bundler'
Bundler.require

configure do
  enable :reloader
end

PORT, PEER_PORT = ARGV.first(2)
set :port, PORT

# BLOCKCHAIN = BlockChain.new
BALANCES = Hash.new {|h,k| h[k] = 0 }
BALANCES['matt'] = 1_000_000

get '/balance' do
  user = params['user']
  "User: #{user} has #{ BALANCES[user].to_i} balance\n"
end

post '/transfers' do
  from = my_params['from'].downcase
  to = my_params['to'].downcase
  amount = my_params['amount'].to_i

  raise unless BALANCES[from] >= amount
  BALANCES[from] -= amount
  BALANCES[to] += amount
  "OK"
end

post '/gossip' do
  their_blockchain = YAML.load(params['blockchain'])
  their_peers = YAML.load(params['peers'])
  update_blockchain(their_blockchain)
  update_peers(their_peers)
  YAML.dump('peers' => $PEERS, 'blockchain' => $BLOCKCHAIN)
end

def my_params(*args)
  @payload ||= JSON.parse(request.body.read)
end
