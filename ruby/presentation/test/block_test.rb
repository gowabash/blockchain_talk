require_relative 'test_helper'
require 'benchmark'

class BlockTest < Minitest::Test
  def test_genesis_creation
    priv1, pub1 = Helper.keys

    genesis = Block.genesis(pub1)
    assert genesis
    assert genesis.valid?
  end

  def test_work_proof
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    fake_hash = '43orkf3okm3eqofdskmfofkm'
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    signature = key.sign(digest, fake_hash)
    t = Transaction.new(pub1, pub2, 100)
    b = Block.new(fake_hash, t, signature)
    b.mine!
    assert b.hash_val.start_with?('000')
  end

  def test_I_can_validate
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    fake_hash = '3245onfr9043moerdfjsklm'
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    signature = key.sign(digest, fake_hash)
    t = Transaction.new(pub1, pub2, 100)

    b = Block.new(fake_hash, t, signature)
    b.mine!
    assert b.valid?

    b = Block.new('not the real hash', t, signature)
    b.mine!
    refute b.valid?

  end


  def x_test_amount_of_work_timings
    orig = $VERBOSE
    $VERBOSE = nil
    priv1, pub1 = Helper.keys
    priv2, pub2 = Helper.keys

    genesis = Block.genesis(pub1)
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(priv1)

    puts "\nTimings for different number of 0's"
    (3..6).each do |val|
      time = Benchmark.measure do
        Block.send(:const_set, :NUM_ZEROES, val)
        signature = key.sign(digest, genesis.hash_val)
        t = Transaction.new(pub1, pub2, 100)
        b = Block.new(genesis, t, signature)
        b.mine!
      end
      puts "#{val} 0's took #{time.real} seconds"
    end
    Block.send(:const_set, :NUM_ZEROES, 3)
    puts ""
    $VERBOSE = orig
  end
end

