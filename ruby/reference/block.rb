require 'digest'

class Block
  attr_reader :prev_block_hash, :transaction
  NUM_ZEROES = 3

  def initialize(prev_block_hash, transaction, signature)
    @nonce = "Satoshi Nakamoto is a very smart man"
    @prev_block_hash = prev_block_hash
    @transaction = transaction
    @count = 1
    @signature = signature
  end

  def self.genesis(to)
    t = Transaction.new(nil,to, 1_000_000)
    Block.new(nil,t,nil)
  end

  def valid?
    return true if @transaction.genesis?
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(@transaction.from)

    tran_valid = key.verify(digest, @signature, @prev_block_hash)
    tran_valid && nonce_valid?
  end

  def mine!
    until nonce_valid?
      @nonce.next!
      @count += 1
    end
  end

  def hash_val
    @the_hash ||= "Not Computed or Genesis Block"
  end

  def to_s
    "Transaction: \n\t#{@transaction}\nNonce: #{@nonce}\nHash: #{hash_val}\nIterations: #{@count}"
  end
  private 

  def hash(contents)
    Digest::SHA256.hexdigest(contents)
  end

  def nonce_valid?
    @the_hash = hash(the_block)
    @the_hash.start_with?("0" * NUM_ZEROES)
  end

  def the_block
    [@transaction.to_s, @prev_block_hash, @nonce].compact.join
  end
end
