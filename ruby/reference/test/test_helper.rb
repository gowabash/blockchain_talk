require 'bundler'
Bundler.require

require 'openssl'
require 'minitest/autorun'

require_relative '../block'
require_relative '../blockchain'
require_relative '../transaction'

Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(:color => true)]

class Helper
  def self.keys
    rsa_key = OpenSSL::PKey::RSA.new(2048)
    private_key = rsa_key.to_pem
    public_key = rsa_key.public_key.to_pem
    return private_key, public_key
  end
end
