class Blockchain
  def initialize(pub_key)
    @chain = [Block.genesis(pub_key)]
  end

  def last_block
    @chain.last
  end

  def add(block)
    validate(block)
    @chain << block
  end

  def count
    @chain.count
  end

  def valid?
    (1..@chain.length - 1).each do |i|
      block = @chain[i]
      prev_block = @chain[i-1]
      valid = block.valid? 
      orderly = prev_block.hash_val == block.prev_block_hash
      valid && orderly
    end
  end

  private

  def in_order?(block)
    block.prev_block_hash == last_block.hash_val
  end

  def validate(block)
    if block.valid?
      if in_order?(block)
        return true
      else
        raise InvalidBlock.new("Previous block is incorrect")
      end
    else
      raise InvalidBlock.new("Your block is not valid")
    end
  end
end

class InvalidBlock < StandardError
end
