require 'openssl'
require 'base64'

class Transaction
  attr_reader :from, :to, :amount

  def initialize(from, to, amount)
    @from = from
    @to = to
    @amount = amount
  end

  def to_s
    "To: #{to}, From: #{from}, Amount: #{amount}"
  end

  def genesis?
    !@from
  end
end
