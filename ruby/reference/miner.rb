class Miner
  attr_reader :blockchain, :peers, :transactions
  def initialize
    peers = []
    transactions = []
  end

  def sync(their_state)
    peers = (their_state.peers + peers).uniq
    transactions = (their_state.transactions + transactions).uniq
  end
end
