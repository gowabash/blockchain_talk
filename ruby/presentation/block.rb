class Block
  GENESIS_HASH = "Not Computed or Genesis Block"
  attr_accessor :nonce, :prev_block_hash
  def initialize(prev_block_hash, transaction, message)
    @prev_block_hash = prev_block_hash
    @transaction = transaction
    @message = message
    @nonce = "Satoshi Nakamoto is a really smart persona"
  end

  def self.genesis(pub_key)
    t = Transaction.new(nil, pub_key, 1000)
    Block.new(nil, t, nil)
  end

  def valid?
    return true if genesis?
    digest = OpenSSL::Digest::SHA256.new
    key = OpenSSL::PKey::RSA.new(@transaction.from)

    tran_valid = key.verify(digest, @message, @prev_block_hash)
    tran_valid && valid_nonce?
  end

  def mine!
    until valid_nonce?
      @nonce.next!
    end
  end

  def hash_val
    @the_hash || GENESIS_HASH
  end

  private

  def genesis?
    !@prev_block_hash
  end

  def hash(contents)
    Digest::SHA256.hexdigest(contents)
  end

  def valid_nonce?
    @the_hash = hash(this_block)
    @the_hash.start_with?("000")
  end

  def this_block
    [@transaction.to_s, @prev_block_hash, @nonce].compact.join
  end

end
